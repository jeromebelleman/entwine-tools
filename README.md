`photos(params, start=0, stop=None, size=64, details=False)`
:   Print photo gallery. It expects the `__params__` dictionary.

`include(path)`
:   Include file contents

`indexposts()`
:   Print index of posts

`ganalytics(dev, trackingid)`
:   Insert Google Analytics

`key(keystroke)`
:   Print keystroke
